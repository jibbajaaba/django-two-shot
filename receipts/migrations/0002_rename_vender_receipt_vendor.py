# Generated by Django 5.0.3 on 2024-03-13 15:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="vender",
            new_name="vendor",
        ),
    ]
